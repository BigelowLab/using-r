# README #

Welcome to the **Using R** public repository at [Bigelow Laboratory for Ocean Science](https://www.bigelow.org/)

### What is this repository for? ###

* Here we store notes, code examples and small(!) data examples.

### How do I get set up? ###

* Install [R](http://www.r-project.org/) and [RStudio](http://www.rstudio.com/)
* Create a local copy of this repository.  Learning to use repositories, like Bitbucket, and git will help you get organize and to play well with others.  See this [tutorial](https://www.atlassian.com/git/tutorials) and this [online resource](http://git-scm.com/) for learning how to **clone** this repository on your local drive.

### What is in here? ###

* Notes are stored on the [wiki](https://bitbucket.org/BigelowLab/using-r/wiki/browse/)
* [Source](https://bitbucket.org/BigelowLab/using-r/src/fcfa7048d52cd68eecdddc6f3c446ce527655aa8?at=master) contains directories of code and data

### Handy resources ###

What is R [in 90 seconds!](http://www.revolutionanalytics.com/what-r)

[Handy cheat sheets!](http://www.rstudio.com/resources/cheatsheets/)

Specialized R-related [searching](http://rseek.org/)

[tryR Gentle Introduction](http://tryr.codeschool.com/)

[Impatient R](http://www.burns-stat.com/documents/tutorials/impatient-r/)

The [World Wild Web](http://lmgtfy.com/?q=introduction+to+R)

[Quick-R](http://www.statmethods.net/)

[R-bloggers](http://www.r-bloggers.com/)

Revolutions Analytics [blog](http://blog.revolutionanalytics.com/)

The R Inferno by [Patrick Burns](http://www.burns-stat.com/documents/books/the-r-inferno/)

## Stuck and can't get your hands on the data?

Download as a zip from [here](https://dl.dropboxusercontent.com/u/8433654/Using-R-data.zip)